import React from 'react';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LogEvent from '../logEvent';

import './style.css';

class FightsHistory extends React.Component {

    generateFightHeader(fight) {
        return (
            <div class="fight-header">
                <div className="fight-header__date">{fight.createdAt}</div>
                <div className="fight-header__versus">
                    {fight.fighter1Name} vs {fight.fighter2Name}
                </div>
            </div>
        );
    }

    generateFightLog(fighter1Name, fighter2Name, log) {
        return log.map(logEvent =>
            <LogEvent
                fighter1Name={fighter1Name}
                fighter2Name={fighter2Name}
                event={logEvent}
            />);
    }

    getFightsHistory() {
        const fights = this.props.fights;
        if (!fights || !fights.length) {
            return <p>There have been no fights yet.</p>
        }

        const history = fights.map(fight => {
            return (
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        {this.generateFightHeader(fight)}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div className="log__wrapper">
                            {this.generateFightLog(fight.fighter1Name, fight.fighter2Name, fight.log)}
                        </div>
                    </ExpansionPanelDetails>

                </ExpansionPanel >
            );
        });

        return (
            <div className="history__wrapper">
                {history}
            </div>
        )
    }
    //

    render() {
        return (
            <div id="history-wrapper">
                <h2 id="history-title">The Fights History:</h2>
                <hr />
                {this.getFightsHistory()}
            </div >
        );
    }
}

export default FightsHistory;