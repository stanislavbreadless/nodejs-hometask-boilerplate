import React from 'react';

import './style.css';

class LogEvent extends React.Component {
    render() {
        const fighter1Name = this.props.fighter1Name;
        const fighter1Shot = this.props.event.fighter1Shot;
        const fighter1Health = this.props.event.fighter1Health;

        const fighter2Name = this.props.fighter2Name;
        const fighter2Shot = this.props.event.fighter2Shot;
        const fighter2Health = this.props.event.fighter2Health;



        return (
            <div className="log-event__wrapper">
                <div className="log-event__first-player">
                    <p className="log-event__name">{fighter1Name}</p>
                    <ul className="log-event__stats-list">
                        <li>👊{fighter1Shot}</li>
                        <li>❤️{fighter1Health}</li>
                    </ul>
                </div>

                <div className="log-event__second-player">
                    <p className="log-event__name">{fighter2Name}</p>
                    <ul className="log-event__stats-list">
                        <li>{fighter2Shot}👊</li>
                        <li>{fighter2Health}❤️</li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default LogEvent;