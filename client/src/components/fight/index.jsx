import React from 'react';

import { getFighters } from '../../services/domainRequest/fightersRequest';
import { getFights, createFight } from '../../services/domainRequest/fightsRequest';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import FightsHistory from '../fightsHistory'
import { Button } from '@material-ui/core';
import { generateFight } from '../../services/fightService';

import './fight.css'

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null,
        fights: []
    };

    getFighterNameById = (id) => {
        const fighters = this.state.fighters;
        const fighter = fighters.filter(item => item.id === id);

        if (!fighter || !fighter[0] || !fighter[0].name)
            return '[deleted]';
        else
            return fighter[0].name;
    }

    addFightersNames = (fight) => {
        return {
            ...fight,
            fighter1Name: this.getFighterNameById(fight.fighter1),
            fighter2Name: this.getFighterNameById(fight.fighter2)
        }
    }

    async componentDidMount() {
        const fighters = await getFighters();
        const rawFights = await getFights();

        if (fighters && !fighters.error) {
            this.setState({ fighters });
        }

        if (rawFights && !rawFights.error) {
            const fights = rawFights.map(fight => this.addFightersNames(fight));

            this.setState({ fights });
        }
    }

    onFightStart = async () => {
        const { fighter1, fighter2 } = this.state;
        if (!fighter1 || !fighter2) {
            return;
        }

        const newRawFight = await createFight(generateFight(fighter1, fighter2));
        if (newRawFight && !newRawFight.error) {
            this.setState({ fights: [...this.state.fights, this.addFightersNames(newRawFight)] });
        }
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({ fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;
        if (!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;
        if (!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    render() {
        const { fighter1, fighter2, fights } = this.state;
        return (
            <div id="wrapper">
                <NewFighter onCreated={this.onCreate} />
                <div id="figh-wrapper">
                    <Fighter selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} />
                    <div className="btn-wrapper">
                        <Button onClick={this.onFightStart} variant="contained" color="primary">Start Fight</Button>
                    </div>
                    <Fighter selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} />
                </div>
                <FightsHistory fights={fights} />
            </div>
        );
    }
}

export default Fight;