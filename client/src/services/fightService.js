
const randInRange = (l, r) => {
    return l + (Math.random() * (r - l));
}

const getAttack = (fighter) => {
    return fighter.power * randInRange(1, 2);
}

const getDefense = (fighter) => {
    return fighter.defense * randInRange(1, 2);
}

const getDamage = (attacker, defender) => {
    const damage = getAttack(attacker) - getDefense(defender);

    return damage > 0 ? damage : 0;
}

const getTurnLog = (fighter1, fighter2, turn) => {
    if (turn % 2 === 0) {
        const damage = getDamage(fighter1, fighter2);
        fighter2.health -= damage;
        if (fighter2.health < 0) {
            fighter2.health = 0;
        }
        return {
            fighter1Shot: damage,
            fighter2Shot: 0,
            fighter1Health: fighter1.health,
            fighter2Health: fighter2.health
        };
    }
    else {
        const damage = getDamage(fighter2, fighter1);
        fighter1.health -= damage;
        if (fighter1.health < 0) {
            fighter1.health = 0;
        }
        return {
            fighter1Shot: 0,
            fighter2Shot: damage,
            fighter1Health: fighter1.health,
            fighter2Health: fighter2.health
        };
    }
}

const createLog = (fighter1, fighter2) => {
    // Shallow copy so that the health of the real 
    // objects is not affected
    fighter1 = { ...fighter1 };
    fighter2 = { ...fighter2 };

    const maxAllowedTurns = 200;
    const log = [];
    let turn = 0;

    while (turn < maxAllowedTurns &&
        fighter1.health > 0 &&
        fighter2.health > 0) {
        log.push(getTurnLog(fighter1, fighter2, turn));
        turn += 1;
    }

    if (turn == maxAllowedTurns) {
        return null;
    }

    return log;
}

export const generateFight = (fighter1, fighter2) => {
    return {
        fighter1: fighter1.id,
        fighter2: fighter2.id,
        log: createLog(fighter1, fighter2)
    };
}