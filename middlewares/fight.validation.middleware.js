const { fight } = require('../models/fight');
const ValidatorService = require('../services/validatorService');


const createLogValid = (log) => {
    if (!Array.prototype.isPrototypeOf(log)) {
        return false;
    }

    return log.every(logItem => {
        return ValidatorService.validateCreate(fight.log[0], logItem);
    });
}

const creationValidators = {
    log: createLogValid
}

const forbidden = new Set(['id']);

const createFightValid = (req, res, next) => {
    // If there was any error before, just skip
    if (res.err) {
        next();
        return;
    }

    if (!ValidatorService.validateCreate(fight, req.body,
        creationValidators, forbidden)) {
        res.err = Error('Fight entity to create is not valid');
        next();
    }

    next();
}


const updateLogValid = (log) => {
    if (!Array.prototype.isPrototypeOf(log)) {
        return false;
    }

    return log.every(logItem => {
        return ValidatorService.validateUpdate(fight.log[0], logItem);
    });
}

const updateValidators = {
    log: updateLogValid
}

const updateFightValid = (req, res, next) => {
    // If there was any error before, just skip
    if (res.err) {
        next();
        return;
    }

    if (!ValidatorService.validateUpdate(fight, req.body, updateValidators, forbidden)) {
        res.err = Error('Fighter entity to update is not valid');
    }
    next();
}

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;