const statusCodes = {
    'User not found': 404,
    'Fighter not found': 404,
    'Fight not found': 404,
    'User entity to create is not valid': 400,
    'Fighter entity to create is not valid': 400,
    'Fight entity to create is not valid': 400,
    'Failed to create user': 400,
    'Failed to create fighter': 400,
    'Failed to create fight': 400,
    'User entity to update is not valid': 400,
    'Fighter entity to update is not valid': 400,
    'Fight entity to update is not valid': 400,
};

const responseMiddleware = (req, res, next) => {
    if (!res.err) {
        res.status(200).send(res.data);
        return;
    }

    res.status(statusCodes[res.err.message] || 400).send({
        error: true,
        message: res.err.message
    });

    next();
}

exports.responseMiddleware = responseMiddleware;