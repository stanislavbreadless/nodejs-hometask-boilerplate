const { user } = require('../models/user');
const ValidatorService = require('../services/validatorService');

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@gmail.com/;

    return re.test(email);
}

const validatePhoneNumber = (phoneNumber) => {
    return phoneNumber.length === 13 &&
        phoneNumber.startsWith('+380') &&
        /* Checks if all the characters except 
           for the first one contain numbers */
        /^\d+$/.test(phoneNumber.slice(1));
}

const validatePassword = (password) => {
    return password.length >= 3;
}

const validators = {
    email: validateEmail,
    phoneNumber: validatePhoneNumber,
    password: validatePassword
}

const forbidden = new Set(['id']);

const createUserValid = (req, res, next) => {
    // If there was any error before, just skip
    if (res.err) {
        next();
        return;
    }

    if (!ValidatorService.validateCreate(user, req.body, validators, forbidden)) {
        res.err = Error('User entity to create is not valid');
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // If there was any error before, just skip
    if (res.err) {
        next();
        return;
    }

    if (!ValidatorService.validateUpdate(user, req.body, validators, forbidden)) {
        res.err = Error('User entity to update is not valid');
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;