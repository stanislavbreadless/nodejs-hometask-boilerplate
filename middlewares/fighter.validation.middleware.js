const { fighter } = require('../models/fighter');
const ValidatorService = require('../services/validatorService');

const validateDefense = (defense) => {
    return defense >= 1 && defense <= 10;
}

const validateHealth = (health) => {
    return health === 100;
}

const validatePower = (power) => {
    return power > 0 && power < 100;
}

const validators = {
    defense: validateDefense,
    health: validateHealth,
    power: validatePower
}

const forbidden = new Set(['id']);
const optional = new Set(['health']);

const createFighterValid = (req, res, next) => {
    // If there was any error before, just skip
    if (res.err) {
        next();
        return;
    }
    if (!req.body.health) {
        req.body.health = 100;
    }

    if (!ValidatorService.validateCreate(fighter, req.body,
        validators, forbidden, optional)) {
        res.err = Error('Fighter entity to create is not valid');
    }


    next();
}

const updateFighterValid = (req, res, next) => {
    // If there was any error before, just skip
    if (res.err) {
        next();
        return;
    }

    if (!ValidatorService.validateUpdate(fighter, req.body, validators, forbidden)) {
        res.err = Error('Fighter entity to update is not valid');
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;