class ValidatorService {
    // In update all those who are not forbidden, are optional by default
    validateUpdate(model, data, validators, forbidden) {
        if (!data)
            return false;
        const hasOnlyModeledKeys = model && Object.keys(data).every(key => model.hasOwnProperty(key));
        const hasOnlyValidatedKeys = !validators || Object.keys(data).every(key =>
            validators.hasOwnProperty(key) ? validators[key](data[key]) : true);
        const hasOnlyAllowedKeys = !forbidden || Object.keys(data).every(key =>
            !forbidden.has(key));

        return hasOnlyModeledKeys &&
            hasOnlyAllowedKeys &&
            hasOnlyValidatedKeys;
    }

    validateCreate(model, data, validators, forbidden, optional) {
        const hasAllNeededKeys = !model || Object.keys(model).every(key => {
            return (forbidden && forbidden.has(key)) ||
                (optional && optional.has(key)) ||
                data.hasOwnProperty(key)
        });

        return this.validateUpdate(model, data, validators, forbidden, optional) &&
            hasAllNeededKeys
        1
    }
}

module.exports = new ValidatorService();