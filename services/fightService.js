const { FightRepository } = require('../repositories/fightRepository');
const { ItemService } = require('./itemService');

class FightService extends ItemService {
    constructor() {
        super(FightRepository, 'Fight');
    }
}

module.exports = new FightService();