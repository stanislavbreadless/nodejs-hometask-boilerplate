const { FighterRepository } = require('../repositories/fighterRepository');
const { ItemService } = require('./itemService');

class FighterService extends ItemService {
    constructor() {
        super(FighterRepository, 'Fighter');
    }
}

module.exports = new FighterService();