const { UserRepository } = require('../repositories/userRepository');
const { ItemService } = require('./itemService');

class UserService extends ItemService {
    constructor() {
        super(UserRepository, 'User');
    }
}

module.exports = new UserService();