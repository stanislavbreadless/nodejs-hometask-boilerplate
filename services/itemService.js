class ItemService {
    constructor(repository, itemName) {
        this.repository = repository;
        this.itemName = itemName;
        this.lowerCaseItemName = itemName.toLowerCase();
    }

    delete(id) {
        const item = this.repository.delete(id);
        if (!item || !item.length || item.length === 0) {
            throw Error(`${this.itemName} not found`);
        }
        return item;
    }

    getAll() {
        const items = this.repository.getAll();
        if (!items) {
            items = [];
        }
        return items;
    }

    getById(id) {
        const fighter = this.search({ id });
        if (!fighter) {
            throw Error(`${this.itemName} not found`);
        }
        return fighter;
    }

    create(data) {
        const item = this.repository.create(data);
        if (!item) {
            throw Error(`Failed to create ${item.lowerCaseItemName}`);
        }
        return item;
    }

    update(id, dataToUpdate) {
        if (!this.getById(id)) {
            throw Error(`${this.itemName} not found`);
        }

        const item = this.repository.update(id, dataToUpdate);

        if (!item) {
            throw Error(`${this.itemName} not found`);
        }
        return item;
    }

    search(search) {
        const item = this.repository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

exports.ItemService = ItemService;