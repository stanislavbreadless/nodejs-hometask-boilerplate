const { Router } = require('express');
const UserService = require('../services/userService');
const initItemRoute = require('./itemRoutes');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');

const router = Router();

initItemRoute(router, UserService, createUserValid, updateUserValid);

module.exports = router;