const { Router } = require('express');
const { responseMiddleware } = require('../middlewares/response.middleware');

const initItemRoute = (router, itemService, createValid, updateValid) => {
    const createItem = (req, res, next) => {
        if (res.err) {
            next();
            return;
        }

        try {
            res.data = itemService.create(req.body);
        }
        catch (err) {
            res.err = err;
        }
        finally {
            next();
        }
    };
    router.post('/', [createValid, createItem, responseMiddleware]);

    const updateItem = (req, res, next) => {
        if (res.err) {
            next();
            return;
        }

        try {
            res.data = itemService.update(req.params.id, req.body);
        }
        catch (err) {
            res.err = err;
        }
        finally {
            next();
        }
    };
    router.put('/:id', [updateValid, updateItem, responseMiddleware])


    router.get('/', (req, res, next) => {
        if (res.err) {
            next();
            return;
        }

        res.data = itemService.getAll();
        next();
    }, responseMiddleware);

    router.get('/:id', (req, res, next) => {
        if (res.err) {
            next();
            return;
        }

        try {
            res.data = itemService.getById(req.params.id);
        }
        catch (err) {
            res.err = err;
        }
        finally {
            next();
        }
    }, responseMiddleware);

    router.delete('/:id', (req, res, next) => {
        if (res.err) {
            next();
            return;
        }

        try {
            res.data = itemService.delete(req.params.id);
        }
        catch (err) {
            res.err = err;
        }
        finally {
            next();
        }
    }, responseMiddleware);

}

module.exports = initItemRoute;