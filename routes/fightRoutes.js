const { Router } = require('express');
const FightService = require('../services/fightService');
const initItemRoute = require('./itemRoutes');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');

const router = Router();

initItemRoute(router, FightService, createFightValid, updateFightValid);

module.exports = router;