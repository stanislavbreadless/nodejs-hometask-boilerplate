const { Router } = require('express');
const FighterService = require('../services/fighterService');
const initItemRoute = require('./itemRoutes');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

initItemRoute(router, FighterService, createFighterValid, updateFighterValid);

module.exports = router;